<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {

 
   // $table->timestamp('ended_at')->nullable();
    


            $table->increments('id');
            $table->string('marca');
            $table->string('placa');
            $table->integer('celda')->default('-1');
            $table->dateTime('started_at')->default(DB::raw('CURRENT_TIMESTAMP'));
           
            $table->timestamps();
        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
