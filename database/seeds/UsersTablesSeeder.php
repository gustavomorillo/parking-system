<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use franciscoParking\User;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'    => 'Francisco',
            'email'    => 'francisco@alegra.com',
            'password'   =>  Hash::make('alegra'),
            'remember_token' =>  str_random(10),
        ]);
    }
}
