<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  
	return view('login');
    //return view('welcome');
});

Route::get('/cars/reporte', 'ReporteController@index');
Route::get('/cars/placa', 'PlacaController@index');

Route::get('/cars/placa', 'PlacaController@index');
Route::get('/search', 'PlacaController@search');


Route::get('/cars/disponibilidad', 'DisponibilidadController@index');









Route::resource('vehiculos', 'VehiculoController');
Route::resource('cars', 'CarController');
Route::resource('placas', 'PlacaController');
Route::resource('disponibilidad', 'DisponibilidadController');


Auth::routes();
Route::get('/main', 'MainController@index');
Route::post('/main/checklogin', 'MainController@checklogin');
Route::get('main/successlogin', 'MainController@successlogin');
Route::get('main/logout', 'MainController@logout');
Route::get('/home', 'HomeController@index')->name('home');


//Route::post('/main/checklogin', 'MainController@checklogin');

//Route::get('welcome', 'MainController@successlogin');
//Route::get('main/logout', 'MainController@logout');

