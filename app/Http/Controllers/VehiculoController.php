<?php

namespace franciscoParking\Http\Controllers;
use franciscoParking\Car;
use franciscoParking\Vehiculo;
use Illuminate\Http\Request;

class VehiculoController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Request $request
        //$request->user()->authorizeRoles('admin');

        $vehiculos = Vehiculo::all();

        return view('vehiculos.index', compact('vehiculos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehiculos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {






                 $min=1;
                 $max=20;

                 $randomcell = rand($min,$max);

            
            $user = \DB::table('vehiculos')->where('celda',$randomcell)->first();

             
               $validateData = $request->validate([
            'marca' => 'required|max: 15',
            'placa' => 'required|max: 10',
        ]);

        $vehiculo = new Vehiculo();
        $vehiculo->marca = $request->input('marca');
        $vehiculo->placa = $request->input('placa');
        $vehiculo->celda = $randomcell;
        
       
            
            if(!$user){
                $vehiculo->save();
        
    return redirect()->route('vehiculos.index', [$vehiculo])->with('status', 'Registro creado correctamente'); 
                
             }
             if($user){
               return redirect()->route('vehiculos.index', [$vehiculo])->with('status', 'Celda ocupada, buscar otra celda');
            }

            




       

        //return 'save';

        //return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $vehiculo = Vehiculo::find($id);
        return view('vehiculos.show', compact('vehiculo'));
        //return view('vehiculos.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd('edit');
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Vehiculo::destroy($id);
        return view('vehiculos.delete');
      
    }
}
