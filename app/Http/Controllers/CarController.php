<?php

namespace franciscoParking\Http\Controllers;
use franciscoParking\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
   


    public function index(Request $request){
        if($request->ajax()){

            $cars = Car::all();
            return response()->json($cars, 200);

            
        }
        return view('cars.index');
    }








     public function store(Request $request){
        if($request->ajax()){
            

              
                 $min=1;
                 $max=20;

                 $randomcell = rand($min,$max);

            
            $user = \DB::table('cars')->where('celda',$randomcell)->first();

             
              
            $car = new Car();
            $car->marca = $request->input('marca');
            $car->placa = $request->input('placa');
            $car->celda = $randomcell;
            

            if(!$user){
                $car->save();
             return response()->json([
                "message" => "Carro creado correctamente.",
                "car" => $car
            ], 200);
             }
             if($user){
               return response()->json([
                "message" => "Duplicado.",
                "car" => $car
            ], 401); 
            }

            }
            

         
    }


        

     public function show($id)
    {


        $car = Car::find($id);
        return view('cars.show', compact('car'));
        //return view('vehiculos.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function destroy($id)
    {
       Car::destroy($id);
        return view('cars.delete');
      
    }
}