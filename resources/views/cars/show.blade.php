@extends('layouts.app')

@section('title', 'Vehiculos')

@section('content')

{{--    @if(isset(Auth::user()->email))
    <div class="alert  success-block" style="text-align: right;">
     <strong>Welcome {{ Auth::user()->email }}</strong>
     <br />
     <a href="{{ url('/main/logout') }}">Logout</a>
    </div>
   @else
    <script>window.location = "/main";</script>
   @endif --}}


<div class="row">
	
@php
 
  $registrado =  $car->started_at;  
   $actual  = date("Y-m-d h:i:sa"); 

  

   $dteStart = new DateTime($registrado); 
   $dteEnd   = new DateTime($actual); 

$dteDiff  = $dteStart->diff($dteEnd); 

@endphp
		

			<div class="col-sm">
	
			<div class="card text-center" style="width: 18rem;margin-top: 35px;">
			  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
			  <div class="card-body" >
			    <h5 class="card-title">Celda</h5>
			    <p class="card-text">{{$car->celda}}</p>
			    <h5 class="card-title">Tiempo Transcurrido</h5>
			    <p class="card-text">@php print $dteDiff->format("%H:%I:%S") @endphp </p>
			    
			    <a href="/cars/" class="btn btn-secondary">Volver</a>


			    <form action="{{ route('cars.destroy', $car->id )}}" method="POST"> 
			    	<input name="_method" type="hidden" value="DELETE">
					{{ csrf_field() }}
				<input type="submit" class="btn btn-danger btn-xs" value="Eliminar"></input>
				</form>

			    
			  </div>
			</div>
			</div>
		

	
	</div>


@endsection