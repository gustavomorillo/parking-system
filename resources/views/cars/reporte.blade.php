@extends('layouts.app')

@section('title', 'Vehiculos')

@section('content')

    @if(isset(Auth::user()->email))
  {{--   <div class="alert  success-block" style="text-align: right;">
     <strong>Welcome {{ Auth::user()->email }}</strong>
     <br />
     <a href="{{ url('/main/logout') }}">Logout</a>
    </div> --}}
   @else
    <script>window.location = "/main";</script>
   @endif 



<div class="col-md-4">
	<form action="/search" method="get">
	
	<div class="form-group" style="margin-top: 25px; margin-bottom: 25px">
	<input type="search" name="search" class="form-control" placeholder="Introduzca Placa" >
	<span class="form-group-btn">
		<button type="submit" class="btn btn-primary" style="margin-top: 5px">Search</button>
	</span>
	</div>

</div>

@php

@endphp

<div class="container">
<h1 style="text-align: center;">Reporte de 5 celdas más ocupadas</h1>
<br>
<table class="table footable" id="1">
  <thead class="thead-dark">
    <tr style="text-align: center;">
      <th scope="col">Celda</th>
      <th scope="col">Marca</th>
      <th scope="col">Placa</th>
      <th scope="col">Tiempo</th>
      <th scope="col">Monto a pagar</th>
      <th scope="col">Eliminar</th>
      
    </tr>
  </thead>
  @php $pila = array(); @endphp
  @foreach($cars as $car)
@php
 
array_push($pila, $car->celda);





$registrado =  $car->started_at;  
$actual  = date("Y-m-d h:i:sa"); 
$actual2 = date('Y-m-d H:i:s',strtotime('-3 hour',strtotime($actual)));
  

   $dteStart = new DateTime($registrado); 
   $dteEnd   = new DateTime($actual2); 

$dteDiff  = $dteStart->diff($dteEnd); 

$hora = $dteDiff->format('%H:%i:%s');

list($horas, $minutos, $segundos) = explode(':', $hora);
$hora_en_segundos = ($horas * 3600 ) + ($minutos * 60 ) + $segundos;



@endphp
  <tbody>
  	
    <tr style="text-align: center;">
      <th scope="row" >{{$car->celda}}</th>
      <td>{{$car->marca}}</td>
      <td>{{$car->placa}}</td>
      <td>@php print $dteDiff->format("%H:%I:%S") @endphp </td>
      <td>@php echo (($hora_en_segundos)*30)." $"; @endphp</td>
      <td><a href="{{route('vehiculos.destroy', $car->id) }}" class="btn btn-danger">Registrar Salida</td>
      
  	</tr>
			
	@endforeach
  @php 



@endphp



	</tbody>

</table>

<a href="/"><button type="button" class="btn btn-secondary top-space"  >Volver</button></a>


</div>


@endsection

