@extends('layouts.app')

@section('title', 'Vehiculos')

@section('content')

    @if(isset(Auth::user()->email))
  {{--   <div class="alert  success-block" style="text-align: right;">
     <strong>Welcome {{ Auth::user()->email }}</strong>
     <br />
     <a href="{{ url('/main/logout') }}">Logout</a>
    </div> --}}
   @else
    <script>window.location = "/main";</script>
   @endif 


<br>
<h1 style="text-align: center;">Disponibilidad de Celdas</h1>
<br>
<br>

@php







@endphp

<div class="container">


  @php $pila = array(); @endphp
  @foreach($cars as $car)
@php
 
array_push($pila, $car->celda);





$registrado =  $car->started_at;  
$actual  = date("Y-m-d h:i:sa"); 
$actual2 = date('Y-m-d H:i:s',strtotime('-3 hour',strtotime($actual)));
  

   $dteStart = new DateTime($registrado); 
   $dteEnd   = new DateTime($actual2); 

$dteDiff  = $dteStart->diff($dteEnd); 

$hora = $dteDiff->format('%H:%i:%s');

list($horas, $minutos, $segundos) = explode(':', $hora);
$hora_en_segundos = ($horas * 3600 ) + ($minutos * 60 ) + $segundos;



@endphp

			
	@endforeach
  @php 



@endphp

<table class="table">
  <thead style="text-align: center;" class="thead-dark">
    <tr>
      <th scope="col">Celda</th>
      <th scope="col">Disponibilidad</th>
    </tr>
  </thead>
  <tbody>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">1</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(1, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>


    <tr style="text-align: center;">
      <th scope="row">2</th>
      <td>@php if (!in_array(2, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">3</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(3, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">4</th>
      <td>@php if (!in_array(4, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">5</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(5, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">6</th>
      <td>@php if (!in_array(6, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">7</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(7, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" >8</th>
      <td>@php if (!in_array(8, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">9</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(9, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">10</th>
      <td>@php if (!in_array(10, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">11</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(11, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">12</th>
      <td>@php if (!in_array(12, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">13</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(13, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" >14</th>
      <td>@php if (!in_array(14, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">15</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(15, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">16</th>
      <td>@php if (!in_array(16, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">17</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(17, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">18</th>
      <td>@php if (!in_array(18, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row" style="background-color: #D0CDD7">19</th>
      <td style="background-color: #D0CDD7">@php if (!in_array(19, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>
    <tr style="text-align: center;">
      <th scope="row">20</th>
      <td>@php if (!in_array(20, $pila)) {
    echo "Disponible";} else {echo "Ocupada"; }  @endphp</td>
    </tr>

  </tbody>
</table>

	</tbody>

</table>

<a href="/"><button type="button" class="btn btn-secondary top-space"  >Volver</button></a>


</div>


@endsection

