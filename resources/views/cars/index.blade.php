 @extends('layouts.app')

@section('content')

<div class="container box">
   <h1 align="center" style="margin-top: 50px;">Francisco's Parking </h1><br />

 {{--   @if(isset(Auth::user()->email))
    <div class="alert  success-block" style="text-align: right;">
     <strong>Welcome {{ Auth::user()->email }}</strong>
     <br />
     <a href="{{ url('/main/logout') }}">Logout</a>
    </div>
   @else
    <script>window.location = "/main";</script>
   @endif --}}
   
   <br />
  </div>


	<add-car-btn></add-car-btn>
	<delete-car-btn></delete-car-btn>

	<cars-component></cars-component>
	<create-car-form></create-car-form>

	<a href="/"><button type="button" class="btn btn-secondary top-space" style="margin-left: 50px" >Volver</button></a>
	  			


@endsection	