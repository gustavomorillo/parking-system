@extends('layouts.app')

@section('title', 'Vehiculos')

@section('content')



                    

                    @if(isset(Auth::user()->email))
                   {{--  <div class="alert  success-block">
                     <strong>Welcome {{ Auth::user()->name }}</strong> 
                    <br />
                    <a href="{{ url('/main/logout') }}">Logout</a> 
                    </div> --}}
                    @else
                    <script>window.location = "/main";</script>
                    @endif

                    
                    

	@if(session('status'))
		<div class="alert alert-success">
		{{ session('status')}}
		</div>
	@endif
	

<a href="/vehiculos/create"><button type="button" class="btn  btn-success" style="margin-top: 50px;"  href="">Registrar Vehiculo</button></a>
<div class="row">


	@foreach($vehiculos as $vehiculo)

		

			<div class="col-sm">
	
			<div class="card text-center" style="width: 18rem;margin-top: 35px;">
			  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
			  <div class="card-body" >
			    <h5 class="card-title">Marca</h5>
			    <p class="card-text">{{$vehiculo->marca}}</p>
			    <h5 class="card-title">Placa</h5>
			    <p class="card-text">{{$vehiculo->placa}}</p>
			    <a href="/vehiculos/{{$vehiculo->id}}" class="btn btn-primary">Mas información</a>
			  </div>
			</div>
			</div>
		

	@endforeach
	</div>
<a href="/" class="btn btn-secondary" style="    margin-top: 50px;">Volver</a>


@endsection
