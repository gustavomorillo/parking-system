@extends('layouts.app')

@section('title', 'Vehiculos')

@section('content')


@if(isset(Auth::user()->email))
                    <div class="alert  success-block">
                   {{--  <strong>Welcome {{ Auth::user()->name }}</strong> --}}
                    <br />
                    {{-- <a href="{{ url('/main/logout') }}">Logout</a> --}}
                    </div>
                    @else
                    <script>window.location = "/main";</script>
                    @endif
<div class="row">
	
@php
 
$registrado =  $vehiculo->started_at;  
$actual  = date("Y-m-d h:i:sa"); 
$actual2 = date('Y-m-d H:i:s',strtotime('-3 hour',strtotime($actual)));
  

   $dteStart = new DateTime($registrado); 
   $dteEnd   = new DateTime($actual2); 

$dteDiff  = $dteStart->diff($dteEnd); 

$hora = $dteDiff->format('%d:%H:%i:%s');

list($dias, $horas, $minutos, $segundos) = explode(':', $hora);
$hora_en_segundos = ($dias * 86400) + ($horas * 3600 ) + ($minutos * 60 ) + $segundos;



@endphp
		

			<div class="col-sm">
	
			<div class="card text-center" style="width: 18rem;margin-top: 35px;">
			  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
			  <div class="card-body" >
			    <h5 class="card-title">Celda</h5>
			    <p class="card-text">{{$vehiculo->celda}}</p>
			    <h5 class="card-title">Tiempo Transcurrido</h5>
			    <p class="card-text">@php print $dteDiff->format("%d Dias %H Horas %I Minutos") @endphp</p>
			    <h5 class="card-text">Monto a Pagar</h5>
			    <p class="card-text"> @php echo (($hora_en_segundos)*30)." $" @endphp</p>
			    <form action="{{ route('vehiculos.destroy', $vehiculo->id )}}" method="POST"> 
			    	<input name="_method" type="hidden" value="DELETE">
			    	<input style="display: none" type="text" name="factura" value="@php echo $hora_en_segundos*30 @endphp">
					{{ csrf_field() }}
				<input type="submit" class="btn btn-danger btn-xs" value="Eliminar y Facturar"></input>

				
				</form>





			    <a style="margin-top: 15px"href="/cars/placa" class="btn btn-secondary">Volver</a>



			    
			  </div>
			</div>
			</div>
		

	
	</div>


@endsection