<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Francisco's Parking</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

                a.button3{
                 display:inline-block;
                 padding:0.3em 1.2em;
                 margin:0 0.3em 0.3em 0;
                 border-radius:2em;
                 box-sizing: border-box;
                 text-decoration:none;
                 font-family:'Roboto',sans-serif;
                 font-weight:300;
                 color:#FFFFFF;
                 background-color:#4eb5f1;
                 text-align:center;
                 transition: all 0.2s;
                }
                a.button3:hover{
                 background-color:#4095c6;
                }
                @media all and (max-width:30em){
                 a.button3{
                  display:block;
                  margin:0.2em auto;
                 }
}
        </style>
    </head>
    <body>
        {{-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif --}}



            <div class="content">




               <br />
<br /><br /><br /><br /><br />

                <img src="https://i.ibb.co/Cvh1Z6B/parqueadero-alegrate.png">

                <div class="links">
                    <br>
                    


                    
                    
                    <a href="/vehiculos/"><button type="button" class="btn btn-primary" href="">Registrar Vehiculo</button></a>
                    <a href="/cars/placa"><button type="button" class="btn btn-primary" href="">Listado de Vehiculos</button></a>
                    <a href="/cars/disponibilidad"><button type="button" class="btn btn-primary" href="">Listado de Celdas</button></a>
                    <a href="/cars/reporte"><button type="button" class="btn btn-primary" href="">Reporte</button></a>
                
                </div>
            {{-- </div> --}}

                <div class="container box">
                    <h3 align="center"></h3><br />

                    @if(isset(Auth::user()->email))
                    <div class="alert  success-block">
                    <strong>Welcome {{ Auth::user()->name }}</strong>
                    <br />
                    <a href="{{ url('/main/logout') }}">Logout</a>
                    </div>
                    @else
                    <script>window.location = "/main";</script>
                    @endif 

                    <br />
                    </div>



        </div>



          <p style="text-align: center;">CopyRight © 2018  All Rights Reserved Diseñado por Gustavo Morillo</p> 
    </body>
</html>
